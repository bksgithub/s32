let http = require("http");

let port = 4000;

let server = http.createServer((request, response) => {

	/*
	a. If the url is http://localhost:4000/, send a response Welcome to Booking System
*/
	if (request.url == "/") {
		response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Welcome to Booking System")
	} 

	/*
	b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!
*/
	else if (request.url == "/profile") {
		response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Welcome to your profile!")
	} 

	/*
	c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
*/
	else if (request.url == "/courses") {
		response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Here's our courses available")
	}

	/*
	d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
	*/
	if (request.url == "/addcourse" && request.method == "POST") {
	response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Add a course to our resources.")}

/*e. If the url is http://localhost:4000/updatecourse, send a response Update a 
course to our resources*/
		if (request.url == "/updatecourse" && request.method == "PUT") {
			response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Update a course to our resources")
		}

/*f. If the url is http://localhost:4000/archivecourses, send a response Archive 
courses to our resources*/
		if (request.url == "/archivecourses" && request.method == "DELETE") {
			response.writeHead(200, {"Content-Type": "text/plain"})

		response.end("Archive courses to our resources")
		}

	}
)

server.listen(port)

console.log(`Server is running at localhost: ${port}`);